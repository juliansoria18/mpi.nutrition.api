﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MPI.Nutrition.Api.Models.Users;
using MPI.Nutrition.Api.Models;

namespace MPI.Nutrition.Api.Authorization
{
    public class CustomOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly UserRepository _userRepository;

        public CustomOAuthProvider()
        {
            var context = new ApplicationDbContext();
            _userRepository = new UserRepository(context);
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            //return base.ValidateClientAuthentication(context);
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            var extraData = await context.Request.ReadFormAsync() as IEnumerable<KeyValuePair<string, string[]>>;

            dynamic user;
            if (context.Password.Equals(ConfigurationManager.AppSettings["MobileSecretKey"]))
            {
                user = _userRepository.Login(context.UserName);
            }
            else
            {
                user = _userRepository.Login(context.UserName, context.Password);
            }

            if (user == null)
            {
                context.Rejected();
                context.SetError("Error de autenticación", "Usuario o contraseña incorrectos.");
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return;
            }

            var identity = new ClaimsIdentity("JWT");
            identity.AddClaim(new Claim("Id", user.Id));
            identity.AddClaim(new Claim("FirstName", user.FirstName ?? "No disponible"));
            identity.AddClaim(new Claim("LastName", user.LastName ?? "No disponible"));
            identity.AddClaim(new Claim("BirthDate", user.BirthDate?.ToString("yyyy-MM-dd") ?? "No disponible"));
            identity.AddClaim(new Claim("UserName", user.UserName ?? "No disponible"));
            identity.AddClaim(new Claim("Photo", user.Photo ?? "No disponible"));
            identity.AddClaim(new Claim("Role", JsonConvert.SerializeObject(user.Role)));
            identity.AddClaim(new Claim("Language", "es"));
            identity.AddClaim(new Claim("UpdatedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            identity.AddClaim(new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
            var ticket = new AuthenticationTicket(identity, null);
            context.Validated(ticket);
        }

        public override Task TokenEndpointResponse(OAuthTokenEndpointResponseContext context)
        {
            foreach (var claim in context.Identity.Claims)
            {
                if (!claim.Type.Contains("http") && !claim.Type.Equals("UserName"))
                {
                    context.AdditionalResponseParameters.Add(claim.Type, claim.Value);
                }
            }
            return base.TokenEndpointResponse(context);
        }
    }
}