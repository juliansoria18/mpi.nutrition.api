﻿using IdentityModel.Tokens;
using Microsoft.Owin.Security;
using System;
using System.Configuration;
using System.IdentityModel.Tokens;

namespace MPI.Nutrition.Api.Authorization
{
    public class CustomJwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        public string Protect(AuthenticationTicket data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var validAudience = ConfigurationManager.AppSettings["ValidAudience"];
            var validIssuer = ConfigurationManager.AppSettings["ValidIssuer"];

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var symmetricKey = Convert.FromBase64String(ConfigurationManager.AppSettings["Secret"]);
            var signingCredentials = new HmacSigningCredentials(symmetricKey);

            var token = new JwtSecurityToken(validIssuer, validAudience, data.Identity.Claims, issued.Value.UtcDateTime, expires.Value.UtcDateTime, signingCredentials);

            var handler = new JwtSecurityTokenHandler();

            var jwt = handler.WriteToken(token);

            return jwt;
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}