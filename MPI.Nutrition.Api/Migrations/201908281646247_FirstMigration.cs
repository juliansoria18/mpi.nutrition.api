namespace MPI.Nutrition.Api.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MultimediaNews",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        NewId = c.String(nullable: false, maxLength: 64),
                        MultimediaId = c.String(nullable: false, maxLength: 64),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MultimediaNew_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Multimedias", t => t.MultimediaId, cascadeDelete: true)
                .ForeignKey("dbo.News", t => t.NewId, cascadeDelete: true)
                .Index(t => t.NewId)
                .Index(t => t.MultimediaId);
            
            CreateTable(
                "dbo.Multimedias",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        Name = c.Int(nullable: false),
                        MultimediaTypeId = c.String(maxLength: 64),
                        Path = c.String(),
                        Duration = c.Int(nullable: false),
                        Description = c.String(),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Multimedia_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MultimediaTypes", t => t.MultimediaTypeId)
                .Index(t => t.MultimediaTypeId);
            
            CreateTable(
                "dbo.MultimediaTypes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MultimediaType_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        Title = c.String(nullable: false, maxLength: 128),
                        SubTitle = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        OrganizationId = c.String(nullable: false, maxLength: 64),
                        NewTypeId = c.String(maxLength: 64),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_New_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.NewTypes", t => t.NewTypeId)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .Index(t => t.OrganizationId)
                .Index(t => t.NewTypeId);
            
            CreateTable(
                "dbo.NewTypes",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        IsGeneral = c.Boolean(nullable: false),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_NewType_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NewUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        UserId = c.String(nullable: false, maxLength: 64),
                        NewId = c.String(nullable: false, maxLength: 64),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_NewUser_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.News", t => t.NewId, cascadeDelete: true)
                .ForeignKey("security.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.NewId);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        PhoneNumber = c.String(maxLength: 100),
                        Photo = c.String(),
                        BirthDate = c.DateTime(),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Person_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrganizationUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        UserId = c.String(nullable: false, maxLength: 64),
                        OrganizationId = c.String(nullable: false, maxLength: 64),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OrganizationUser_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Organizations", t => t.OrganizationId, cascadeDelete: true)
                .ForeignKey("security.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.OrganizationId);
            
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        Organization = c.String(maxLength: 100),
                        Color = c.String(),
                        Icon = c.String(),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Organization_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "security.Roles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Role_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "security.Permissions",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        Name = c.String(nullable: false, maxLength: 128),
                        Description = c.String(maxLength: 512),
                        Ordering = c.Int(nullable: false),
                        RegisterDate = c.DateTime(),
                        RegisterBy = c.String(maxLength: 128),
                        UpdatedDate = c.DateTime(),
                        UpdatedBy = c.String(maxLength: 128),
                        DeletedDate = c.DateTime(),
                        DeletedBy = c.String(maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Permission_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PermissionRoles",
                c => new
                    {
                        Permission_Id = c.String(nullable: false, maxLength: 64),
                        Role_Id = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => new { t.Permission_Id, t.Role_Id })
                .ForeignKey("security.Permissions", t => t.Permission_Id, cascadeDelete: true)
                .ForeignKey("security.Roles", t => t.Role_Id, cascadeDelete: true)
                .Index(t => t.Permission_Id)
                .Index(t => t.Role_Id);
            
            CreateTable(
                "security.Users",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 64),
                        UserName = c.String(nullable: false, maxLength: 128),
                        Password = c.String(nullable: false, maxLength: 128),
                        FcmToken = c.String(),
                        RoleId = c.String(nullable: false, maxLength: 64),
                        CountryCode = c.String(),
                        AreaCode = c.String(),
                        LocalPhoneNumber = c.String(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_User_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Persons", t => t.Id)
                .ForeignKey("security.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("security.Users", "RoleId", "security.Roles");
            DropForeignKey("security.Users", "Id", "dbo.Persons");
            DropForeignKey("dbo.PermissionRoles", "Role_Id", "security.Roles");
            DropForeignKey("dbo.PermissionRoles", "Permission_Id", "security.Permissions");
            DropForeignKey("dbo.OrganizationUsers", "UserId", "security.Users");
            DropForeignKey("dbo.OrganizationUsers", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.News", "OrganizationId", "dbo.Organizations");
            DropForeignKey("dbo.NewUsers", "UserId", "security.Users");
            DropForeignKey("dbo.NewUsers", "NewId", "dbo.News");
            DropForeignKey("dbo.News", "NewTypeId", "dbo.NewTypes");
            DropForeignKey("dbo.MultimediaNews", "NewId", "dbo.News");
            DropForeignKey("dbo.Multimedias", "MultimediaTypeId", "dbo.MultimediaTypes");
            DropForeignKey("dbo.MultimediaNews", "MultimediaId", "dbo.Multimedias");
            DropIndex("security.Users", new[] { "RoleId" });
            DropIndex("security.Users", new[] { "Id" });
            DropIndex("dbo.PermissionRoles", new[] { "Role_Id" });
            DropIndex("dbo.PermissionRoles", new[] { "Permission_Id" });
            DropIndex("dbo.OrganizationUsers", new[] { "OrganizationId" });
            DropIndex("dbo.OrganizationUsers", new[] { "UserId" });
            DropIndex("dbo.NewUsers", new[] { "NewId" });
            DropIndex("dbo.NewUsers", new[] { "UserId" });
            DropIndex("dbo.News", new[] { "NewTypeId" });
            DropIndex("dbo.News", new[] { "OrganizationId" });
            DropIndex("dbo.Multimedias", new[] { "MultimediaTypeId" });
            DropIndex("dbo.MultimediaNews", new[] { "MultimediaId" });
            DropIndex("dbo.MultimediaNews", new[] { "NewId" });
            DropTable("security.Users",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_User_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.PermissionRoles");
            DropTable("security.Permissions",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Permission_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("security.Roles",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Role_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Organizations",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Organization_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.OrganizationUsers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OrganizationUser_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Persons",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Person_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.NewUsers",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_NewUser_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.NewTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_NewType_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.News",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_New_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MultimediaTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MultimediaType_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Multimedias",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Multimedia_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.MultimediaNews",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_MultimediaNew_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
