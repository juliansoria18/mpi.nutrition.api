﻿using Microsoft.AspNet.Identity;
using MPI.Nutrition.Api.Models;
using MPI.Nutrition.Api.Models.Users;
using System.Data.Entity.Migrations;
using System.Linq;

namespace MPI.Nutrition.Api.Migrations.Seed
{
    public class UserSeed
    {
        private static UserSeed _instance;
        private readonly ApplicationDbContext _context;

        public UserSeed(ApplicationDbContext context)
        {
            _context = context;
        }

        public static UserSeed Instance(ApplicationDbContext context)
        {
            return _instance ?? (_instance = new UserSeed(context));
        }

        public void Seed()
        {
            if (!_context.Users.Any(u => u.Id.Equals("superadmin")))
            {
                var passwordHasher = new PasswordHasher();
                var user = new User
                {
                    Id = "superadmin",
                    UserName = "superadmin",
                    Password = passwordHasher.HashPassword("123456"),
                    FirstName = "Super",
                    LastName = "Admin",
                    RoleId = "superadmin",
                };
                _context.Users.AddOrUpdate(e => e.Id, user);
                _context.SaveChanges();
            }
            else
            {
                var user = _context.Users.FirstOrDefault(e => e.Id.Equals("superadmin"));
                if (user == null) return;
                user.UserName = "superadmin";
                _context.Users.AddOrUpdate(e => e.Id, user);
                _context.SaveChanges();
            }
        }
    }
}