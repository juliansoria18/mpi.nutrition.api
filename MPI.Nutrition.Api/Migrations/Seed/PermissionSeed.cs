﻿using System.Data.Entity.Migrations;
using MPI.Nutrition.Api.Models;
using MPI.Nutrition.Api.Models.Permissions;

namespace MPI.Nutrition.Api.Migrations.Seed
{
    public class PermissionSeed
    {
        private static PermissionSeed _instance;
        private readonly ApplicationDbContext _context;

        public PermissionSeed(ApplicationDbContext context)
        {
            _context = context;
        }

        public static PermissionSeed Instance(ApplicationDbContext context)
        {
            return _instance ?? (_instance = new PermissionSeed(context));
        }

        public void Seed()
        {
            _context.Permissions.AddOrUpdate(p => p.Id,
                new Permission { Id = "PAGES_HOME", Name = "Inicio", Description = "Inicio", Ordering = 100 },

                new Permission { Id = "PAGES_MANAGEMENT", Name = "Administración", Description = "Administración", Ordering = 200 },

                new Permission { Id = "PAGES_MANAGEMENT_COMPUTERS", Name = "Administración > Computadoras", Description = "Administración > Computadoras", Ordering = 300 },
                new Permission { Id = "PAGES_MANAGEMENT_COMPUTERS_ADD", Name = "Administración > Computadoras > Agregar Computadora", Description = "Administración > Computadoras > Agregar Computadora", Ordering = 301 },
                new Permission { Id = "PAGES_MANAGEMENT_COMPUTERS_EDIT", Name = "Administración > Computadoras > Editar Computadora", Description = "Administración > Computadoras > Editar Computadora", Ordering = 302 },
                new Permission { Id = "PAGES_MANAGEMENT_COMPUTERS_DELETE", Name = "Administración > Computadoras > Eliminar Computadora", Description = "Administración > Computadoras > Eliminar Computadora", Ordering = 303 },

                new Permission { Id = "PAGES_MANAGEMENT_SECTOR_TYPES", Name = "Administración > Tipos de Sectores", Description = "Administración > Tipos de Sectores", Ordering = 310 },
                new Permission { Id = "PAGES_MANAGEMENT_SECTOR_TYPES_ADD", Name = "Administración > Tipos de Sectores > Agregar Tipo de Sector", Description = "Administración > Tipos de Sectores > Agregar Tipo de Sector", Ordering = 311 },
                new Permission { Id = "PAGES_MANAGEMENT_SECTOR_TYPES_EDIT", Name = "Administración > Tipos de Sectores > Editar Tipo de Sector", Description = "Administración > Tipos de Sectores > Editar Tipo de Sector", Ordering = 312 },
                new Permission { Id = "PAGES_MANAGEMENT_SECTOR_TYPES_DELETE", Name = "Administración > Tipos de Sectores > Eliminar Tipo de Sector", Description = "Administración > Tipos de Sectores > Eliminar Tipo de Sector", Ordering = 313 },

                new Permission { Id = "PAGES_SECURITY", Name = "Seguridad", Description = "Seguridad", Ordering = 400 },

                new Permission { Id = "PAGES_SECURITY_ROLES_AND_PERMISSIONS", Name = "Seguridad > Roles y Permisos", Description = "Seguridad > Roles y Permisos", Ordering = 500 },
                new Permission { Id = "PAGES_SECURITY_ROLES_AND_PERMISSIONS_ADD", Name = "Seguridad > Roles y Permisos > Agregar Rol", Description = "Seguridad > Roles y Permisos > Agregar Rol", Ordering = 501 },
                new Permission { Id = "PAGES_SECURITY_ROLES_AND_PERMISSIONS_EDIT", Name = "Seguridad > Roles y Permisos > Editar Rol", Description = "Seguridad > Roles y Permisos > Editar Rol", Ordering = 502 },
                new Permission { Id = "PAGES_SECURITY_ROLES_AND_PERMISSIONS_DELETE", Name = "Seguridad > Roles y Permisos > Eliminar Rol", Description = "Seguridad > Roles y Permisos > Eliminar Rol", Ordering = 503 }
              );

            _context.SaveChanges();
        }
    }
}