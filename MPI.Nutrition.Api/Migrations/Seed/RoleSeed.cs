﻿using MPI.Nutrition.Api.Models;
using MPI.Nutrition.Api.Models.Roles;
using System.Data.Entity.Migrations;
using System.Linq;

namespace MPI.Nutrition.Api.Migrations.Seed
{
    public class RoleSeed
    {
        private static RoleSeed _instance;
        private readonly ApplicationDbContext _context;

        public RoleSeed(ApplicationDbContext context)
        {
            _context = context;
        }

        public static RoleSeed Instance(ApplicationDbContext context)
        {
            return _instance ?? (_instance = new RoleSeed(context));
        }

        public void Seed()
        {
            // Creo o actualizo el rol Super Admin
            var permissions = _context.Permissions.ToList();

            if (!_context.Roles.Any(ar => ar.Id.Equals("superadmin")))
            {
                var newRole = new Role
                {
                    Id = "superadmin",
                    Name = "Super Admin",
                    Description = "Super Admin",
                    Permissions = permissions
                };

                _context.Roles.AddOrUpdate(e => e.Id, newRole);
            }
            else
            {
                var role = _context.Roles.FirstOrDefault(e => e.Id.Equals("superadmin"));
                role.Permissions.Clear();
                role.Permissions = permissions;

                _context.Roles.AddOrUpdate(e => e.Id, role);
            }

            _context.SaveChanges();
        }
    }
}