﻿using System.Web.Http;
using MPI.Nutrition.Api.App_Start;
using MPI.Nutrition.Api.Models;
using MPI.Nutrition.Api.Models.MultimediaNews;
using MPI.Nutrition.Api.Models.Multimedias;
using MPI.Nutrition.Api.Models.MultimediaTypes;
using MPI.Nutrition.Api.Models.News;
using MPI.Nutrition.Api.Models.NewTypes;
using MPI.Nutrition.Api.Models.NewUsers;
using MPI.Nutrition.Api.Models.Organizations;
using MPI.Nutrition.Api.Models.OrganizationUsers;
using MPI.Nutrition.Api.Models.Permissions;
using MPI.Nutrition.Api.Models.Persons;
using MPI.Nutrition.Api.Models.Roles;
using MPI.Nutrition.Api.Models.Users;
using MPI.Nutrition.Api.Resolvers;
using Unity;
using Unity.Lifetime;

namespace MPI.Nutrition.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración y servicios de API web

            // Rutas de API web
            config.MapHttpAttributeRoutes(new InheritanceDirectRouteProvider());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var container = new UnityContainer();

            container.RegisterType(typeof(IRepository<>), typeof(Repository<>), new HierarchicalLifetimeManager());
            container.RegisterType<IPermissionRepository, PermissionRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IMultimediaRepository, MultimediaRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IMultimediaNewRepository, MultimediaNewRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IMultimediaTypeRepository, MultimediaTypeRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<INewRepository, NewRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<INewTypeRepository, NewTypeRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IOrganizationRepository, OrganizationRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IPersonRepository, PersonRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IRoleRepository, RoleRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<INewUserRepository, NewUserRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IOrganizationUserRepository, OrganizationUserRepository>(new HierarchicalLifetimeManager());

            config.DependencyResolver = new UnityResolver(container);

            config.Filters.Add(new ValidateModelAttribute());
        }
    }
}
