﻿using System;
using System.Configuration;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using MPI.Nutrition.Api.Authorization;
using MPI.Nutrition.Api.Models;
using Owin;

[assembly: OwinStartup(typeof(MPI.Nutrition.Api.Startup))]

namespace MPI.Nutrition.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            var config = new HttpConfiguration();
            WebApiConfig.Register(config);

            AutoMapperConfiguration.Initialize();
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            // OAuth 2.0 Bearer Access Token Generation
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                //For Dev enviroment only (on production should be AllowInsecureHttp = false)
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/auth/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(int.Parse(ConfigurationManager.AppSettings["AccessTokenExpireTimeInMinutes"])),
                Provider = new CustomOAuthProvider(),
                AccessTokenFormat = new CustomJwtFormat()
            });

            var validAudience = ConfigurationManager.AppSettings["ValidAudience"];
            var validIssuer = ConfigurationManager.AppSettings["ValidIssuer"];
            var secret = TextEncodings.Base64Url.Decode(ConfigurationManager.AppSettings["Secret"]);

            app.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { validAudience },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                    new SymmetricKeyIssuerSecurityTokenProvider(validIssuer, secret)
                }
            });
        }
    }
}
