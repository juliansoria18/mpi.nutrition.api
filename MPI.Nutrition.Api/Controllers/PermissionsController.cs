﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using MPI.Nutrition.Api.Models;
using MPI.Nutrition.Api.Models.Permissions;

namespace MPI.Nutrition.Api.Controllers
{
    [RoutePrefix("api/Permissions")]
    public class PermissionsController : BaseController<Permission>
    {
        private readonly IRepository<Permission> _repository;
        private readonly IPermissionRepository _permissionRepository;

        public PermissionsController(
            IRepository<Permission> repository,
            IPermissionRepository permissionRepository) : base(repository)
        {
            _repository = repository;
            _permissionRepository = permissionRepository;
        }

        [HttpGet, Route("name/{name}")]
        public IEnumerable<Permission> Get(string name)
        {
            return _repository.Get(e => e.Name.Contains(name));
        }

        [HttpGet, Route("async/name/{name}")]
        public async Task<IEnumerable<Permission>> GetAsync(string name)
        {
            return await _repository.GetAsync(e => e.Name.Contains(name));
        }

        /// <summary>
        /// Ejemplo:
        /// {
        ///     "Page": {
        ///         "PageNumber": 1,
        ///         "Top": 5
        ///     },
        ///     "Order": {
        ///         "By": "Name",
        ///         "Direction": 2
        ///     },
        ///     "Filter": {
        ///         "Value": "Computadora"
        ///     }
        /// }
        /// </summary>
        /// <param name="queryDto"></param>
        /// <returns></returns>
    }
}
