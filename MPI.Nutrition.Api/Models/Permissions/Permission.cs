﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MPI.Nutrition.Api.Models.Roles;
using Newtonsoft.Json;

namespace MPI.Nutrition.Api.Models.Permissions
{
    [Table("security.Permissions")]

    public class Permission : Base
    {
        public Permission()
        {
            Roles = new HashSet<Role>();
        }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public int Ordering { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}