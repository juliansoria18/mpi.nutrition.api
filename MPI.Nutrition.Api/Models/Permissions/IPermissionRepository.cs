﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MPI.Nutrition.Api.Models.Permissions
{
    public interface IPermissionRepository : IRepository<Permission>
    {
    }
}
