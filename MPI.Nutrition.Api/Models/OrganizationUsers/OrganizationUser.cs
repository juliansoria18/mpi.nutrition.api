﻿using MPI.Nutrition.Api.Models.Organizations;
using MPI.Nutrition.Api.Models.Users;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.OrganizationUsers
{
    [Table("OrganizationUsers")]
    public class OrganizationUser : Base
    {
        public OrganizationUser()
        {
        }
        [Required]
        public string UserId { get; set; }

        public virtual User User { get; set; }

        [Required]
        public string OrganizationId { get; set; }

        [JsonIgnore]
        public virtual Organization Organization { get; set; }
    }
}