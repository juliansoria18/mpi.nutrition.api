﻿namespace MPI.Nutrition.Api.Models.OrganizationUsers
{
    public class OrganizationUserRepository : Repository<OrganizationUser>, IOrganizationUserRepository
    {
        public OrganizationUserRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}