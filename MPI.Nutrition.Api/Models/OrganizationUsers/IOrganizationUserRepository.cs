﻿namespace MPI.Nutrition.Api.Models.OrganizationUsers
{
    public interface IOrganizationUserRepository : IRepository<OrganizationUser>
    {
        
    }
}
