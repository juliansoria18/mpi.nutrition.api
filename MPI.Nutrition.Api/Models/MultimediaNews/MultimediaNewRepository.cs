﻿namespace MPI.Nutrition.Api.Models.MultimediaNews
{
    public class MultimediaNewRepository : Repository<MultimediaNew>, IMultimediaNewRepository
    {
        public MultimediaNewRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}