﻿using MPI.Nutrition.Api.Models.Multimedias;
using MPI.Nutrition.Api.Models.News;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.MultimediaNews
{
    [Table("MultimediaNews")]
    public class MultimediaNew : Base
    {
        public MultimediaNew()
        {
        }
        [Required]
        public string NewId { get; set; }

        public virtual New New { get; set; }

        [Required]
        public string MultimediaId { get; set; }

        [JsonIgnore]
        public virtual Multimedia Multimedia { get; set; }
    }
}