﻿using EntityFramework.DynamicFilters;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Security.Claims;
using System.Web;
using MPI.Nutrition.Api.Models.Permissions;
using MPI.Nutrition.Api.Models.MultimediaNews;
using MPI.Nutrition.Api.Models.Multimedias;
using MPI.Nutrition.Api.Models.MultimediaTypes;
using MPI.Nutrition.Api.Models.News;
using MPI.Nutrition.Api.Models.NewTypes;
using MPI.Nutrition.Api.Models.Organizations;
using MPI.Nutrition.Api.Models.Persons;
using MPI.Nutrition.Api.Models.Roles;
using MPI.Nutrition.Api.Models.Users;
using MPI.Nutrition.Api.Models.NewUsers;
using MPI.Nutrition.Api.Models.OrganizationUsers;

namespace MPI.Nutrition.Api.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("name=ApplicationDbContext")
        {
        }

        public DbSet<Permission> Permissions { get; set; }
        public DbSet<MultimediaNew> MultimediaNews { get; set; }
        public DbSet<Multimedia> Multimedias { get; set; }
        public DbSet<MultimediaType> MultimediaTypes { get; set; }
        public DbSet<New> News { get; set; }
        public DbSet<NewType> NewTypes { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<NewUser> NewUsers { get; set; }
        public DbSet<OrganizationUser> OrganizationUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Filter("IsDeleted", (Base d) => d.IsDeleted, false);
        }

        public override int SaveChanges()
        {
            // Obtengo el código de usuario que esta logueado actualmente a través del token de autenticación
            var claimsIdentity = HttpContext.Current?.User?.Identity as ClaimsIdentity;
            var currentUser = !string.IsNullOrEmpty(claimsIdentity?.FindFirst(ClaimTypes.NameIdentifier)?.Value) ? claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value : "Anonymous";

            foreach (var entry in ChangeTracker.Entries<Base>())
            {
                if (entry.State == EntityState.Added || entry.State == EntityState.Modified)
                {
                    // pupulate created date and created by columns for
                    // newly added record.
                    if (entry.State == EntityState.Added)
                    {
                        if (string.IsNullOrEmpty(entry.Entity.Id))
                        {
                            entry.Entity.Id = Guid.NewGuid().ToString();
                        }
                        entry.Entity.RegisterDate = DateTime.UtcNow;
                        entry.Entity.RegisterBy = currentUser;
                    }
                    else
                    {
                        // we also want to make sure that code is not inadvertly
                        // modifying created date and created by columns 
                        //auditableEntity.Property(p => p.Id).IsModified = false;
                        entry.Property(p => p.RegisterDate).IsModified = false;
                        entry.Property(p => p.RegisterBy).IsModified = false;
                    }

                    // modify updated date and updated by column for 
                    // adds of updates.
                    //entry.Entity.Version = BitConverter.GetBytes(DateTime.UtcNow.Ticks);
                    entry.Entity.UpdatedDate = DateTime.UtcNow;
                    entry.Entity.UpdatedBy = currentUser;
                }

                if (entry.State == EntityState.Deleted)
                {
                    SoftDelete(entry, this, currentUser);
                }
            }
            return base.SaveChanges();
        }

        public void SoftDelete(DbEntityEntry entry, ApplicationDbContext context, string deletedBy)
        {
            //Copy orginal values to current values
            var propertyNames = entry.OriginalValues.PropertyNames;
            foreach (var propertyName in propertyNames)
            {
                entry.Property(propertyName).CurrentValue = entry.OriginalValues[propertyName];
            }
            entry.Property("IsDeleted").CurrentValue = true;
            entry.Property("DeletedDate").CurrentValue = DateTime.UtcNow;
            entry.Property("DeletedBy").CurrentValue = deletedBy;
            // prevent hard delete – use modified instead
            entry.State = EntityState.Modified;
        }
    }
}