﻿using MPI.Nutrition.Api.Models.News;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.NewTypes
{
    [Table("NewTypes")]
    public class NewType : Base
    {
        public NewType()
        {
            News = new HashSet<New>();
        }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public bool IsGeneral { get; set; }

        public virtual ICollection<New> News { get; set; }
    }
}