﻿namespace MPI.Nutrition.Api.Models.NewTypes
{
    public interface INewTypeRepository : IRepository<NewType>
    {
        
    }
}
