﻿namespace MPI.Nutrition.Api.Models.NewTypes
{
    public class NewTypeRepository : Repository<NewType>, INewTypeRepository
    {
        public NewTypeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}