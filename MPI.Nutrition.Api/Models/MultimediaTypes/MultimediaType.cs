﻿using MPI.Nutrition.Api.Models.Multimedias;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.MultimediaTypes
{
    [Table("MultimediaTypes")]
    public class MultimediaType : Base
    {
        public MultimediaType()
        {
            Multimedias = new HashSet<Multimedia>();
        }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public virtual ICollection<Multimedia> Multimedias { get; set; }
    }
}