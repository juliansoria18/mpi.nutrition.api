﻿namespace MPI.Nutrition.Api.Models.MultimediaTypes
{
    public interface IMultimediaTypeRepository : IRepository<MultimediaType>
    {
        
    }
}
