﻿namespace MPI.Nutrition.Api.Models.MultimediaTypes
{
    public class MultimediaTypeRepository : Repository<MultimediaType>, IMultimediaTypeRepository
    {
        public MultimediaTypeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}