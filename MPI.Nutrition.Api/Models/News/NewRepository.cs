﻿namespace MPI.Nutrition.Api.Models.News
{
    public class NewRepository : Repository<New>, INewRepository
    {
        public NewRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}