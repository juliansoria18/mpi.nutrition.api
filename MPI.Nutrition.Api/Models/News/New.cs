﻿using MPI.Nutrition.Api.Models.MultimediaNews;
using MPI.Nutrition.Api.Models.NewTypes;
using MPI.Nutrition.Api.Models.NewUsers;
using MPI.Nutrition.Api.Models.Organizations;
using MPI.Nutrition.Api.Models.Users;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.News
{
    [Table("News")]
    public class New : Base
    {
        public New()
        {
            MultimediaNews = new HashSet<MultimediaNew>();
            NewUsers = new HashSet<NewUser>();
        }

        [Required]
        [StringLength(128)]
        public string Title { get; set; }

        [Required]
        [StringLength(128)]
        public string SubTitle { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        [Required]
        [StringLength(64)]
        public string OrganizationId { get; set; }

        [JsonIgnore]
        public virtual Organization Organization { get; set; }

        public string NewTypeId { get; set; }

        [JsonIgnore]
        public virtual NewType NewType { get; set; }

        [JsonIgnore]
        public virtual ICollection<MultimediaNew> MultimediaNews { get; set; }

        [JsonIgnore]
        public virtual ICollection<NewUser> NewUsers { get; set; }
    }
}