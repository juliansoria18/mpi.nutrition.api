﻿using MPI.Nutrition.Api.Models.Permissions;
using MPI.Nutrition.Api.Models.Users;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.Roles
{
    [Table("security.Roles")]
    public class Role : Base
    {
        public Role()
        {
            Permissions = new HashSet<Permission>();
            Users = new HashSet<User>();
        }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public virtual ICollection<Permission> Permissions { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}