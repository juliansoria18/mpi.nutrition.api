﻿using MPI.Nutrition.Api.Models.News;
using MPI.Nutrition.Api.Models.Users;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.NewUsers
{
    [Table("NewUsers")]
    public class NewUser : Base
    {
        public NewUser()
        {
        }
        [Required]
        public string UserId { get; set; }

        public virtual User User { get; set; }

        [Required]
        public string NewId { get; set; }

        [JsonIgnore]
        public virtual New New { get; set; }
    }
}