﻿namespace MPI.Nutrition.Api.Models.NewUsers
{
    public class NewUserRepository : Repository<NewUser>, INewUserRepository
    {
        public NewUserRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}