﻿using AutoMapper;

namespace MPI.Nutrition.Api.Models
{
    public static class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
            });
        }
    }
}