﻿using Microsoft.AspNet.Identity;
using System.Linq;

namespace MPI.Nutrition.Api.Models.Users
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
        }

        public dynamic Login(string phoneNumber)
        {
            return Context.Users.Where(e => e.PhoneNumber != null && e.PhoneNumber.Equals(phoneNumber)).Select(e => new
            {
                e.Id,
                e.FirstName,
                e.LastName,
                e.BirthDate,
                e.UserName,
                e.Photo,
                Role = new
                {
                    e.Role.Id,
                    e.Role.Name,
                    Permissions = e.Role.Permissions.Select(p => new
                    {
                        p.Id,
                        p.Name,
                        p.Description,
                        p.Ordering,
                        IsGranted = true
                    }).ToList()
                }
            }).FirstOrDefault();
        }

        public dynamic Login(string username, string password)
        {
            var user = Context.Users.Where(e => e.UserName.Equals(username)).Select(e => new
            {
                e.Id,
                e.FirstName,
                e.LastName,
                e.BirthDate,
                e.UserName,
                e.Photo,
                Role = new
                {
                    e.Role.Id,
                    e.Role.Name,
                    Permissions = e.Role.Permissions.Select(p => new
                    {
                        p.Id,
                        p.Name,
                        p.Description,
                        p.Ordering,
                        IsGranted = true
                    }).ToList()
                },
                e.Password,
            }).FirstOrDefault();

            if (user == null)
            {
                return null;
            }

            var passwordHasher = new PasswordHasher();
            var verifyHashedPassword = passwordHasher.VerifyHashedPassword(user.Password, password);
            return verifyHashedPassword == PasswordVerificationResult.Success ? user : null;
        }
    }
}