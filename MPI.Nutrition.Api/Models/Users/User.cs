﻿using MPI.Nutrition.Api.Models.News;
using MPI.Nutrition.Api.Models.NewUsers;
using MPI.Nutrition.Api.Models.OrganizationUsers;
using MPI.Nutrition.Api.Models.Persons;
using MPI.Nutrition.Api.Models.Roles;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.Users
{
    [Table("security.Users")]
    public class User : Person
    {
        public User()
        {
            NewUsers = new HashSet<NewUser>();
            OrganizationUsers = new HashSet<OrganizationUser>();
        }

        [Required]
        [StringLength(128)]
        public string UserName { get; set; }

        [Required]
        [StringLength(128)]
        public string Password { get; set; }

        public string FcmToken { get; set; }

        [Required]
        [StringLength(64)]
        public string RoleId { get; set; }

        [JsonIgnore]
        public virtual Role Role { get; set; }

        public string CountryCode { get; set; }

        public string AreaCode { get; set; }

        public string LocalPhoneNumber { get; set; }

        [JsonIgnore]
        public virtual ICollection<NewUser> NewUsers { get; set; }

        [JsonIgnore]
        public virtual ICollection<OrganizationUser> OrganizationUsers { get; set; }
    }
}