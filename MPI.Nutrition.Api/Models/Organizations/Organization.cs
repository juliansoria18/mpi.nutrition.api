﻿using MPI.Nutrition.Api.Models.News;
using MPI.Nutrition.Api.Models.OrganizationUsers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.Organizations
{
    [Table("Organizations")]
    public class Organization : Base
    {
        public Organization()
        {
            News = new HashSet<New>();
            OrganizationUsers = new HashSet<OrganizationUser>();
        }

        [Column("Organization")]
        [StringLength(100)]
        public string OrganizationName { get; set; }

        public string Color { get; set; }

        public string Icon { get; set; }

        public virtual ICollection<New> News { get; set; }

        [JsonIgnore]
        public virtual ICollection<OrganizationUser> OrganizationUsers { get; set; }
    }
}