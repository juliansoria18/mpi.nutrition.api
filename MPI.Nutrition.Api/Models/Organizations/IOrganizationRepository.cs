﻿namespace MPI.Nutrition.Api.Models.Organizations
{
    public interface IOrganizationRepository : IRepository<Organization>
    {
        
    }
}
