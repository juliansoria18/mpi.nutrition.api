﻿namespace MPI.Nutrition.Api.Models.Multimedias
{
    public class MultimediaRepository : Repository<Multimedia>, IMultimediaRepository
    {
        public MultimediaRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}