﻿using MPI.Nutrition.Api.Models.MultimediaNews;
using MPI.Nutrition.Api.Models.MultimediaTypes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MPI.Nutrition.Api.Models.Multimedias
{
    [Table("Multimedias")]
    public class Multimedia : Base
    {
        public Multimedia()
        {
            MultimediaNews = new HashSet<MultimediaNew>();
        }

        public int Name { get; set; }

        public string MultimediaTypeId { get; set; }

        [JsonIgnore]
        public virtual MultimediaType MultimediaType { get; set; }

        public string Path { get; set; }

        public int Duration { get; set; }

        public string Description { get; set; }

        [JsonIgnore]
        public virtual ICollection<MultimediaNew> MultimediaNews { get; set; }
    }
}